/*
Copyright (C) 2015 Fabrizio Curcio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

package main

import (
	"flag"
	"encoding/json"
	"encoding/binary"
	"io/ioutil"
	"fmt"
	"log"
	"os"
	"strconv"
	"sort"
	"path/filepath"
)

var (
	config = flag.String("map-file", "pch_7series_c216.json", "address map configuration file")
	saveTemp = flag.Bool("save-dumps", false, "save partial dumps")
	outDir = flag.String("out-dir", "output", "output directory")
	outDump = flag.String("filename", "biosDump.bin", "dump name")
	probeOnly = flag.Bool("probe-only", false, "check without dump anything")
	biosSegments = make(map[int][]byte)
)

const (
	devMem = "/dev/mem"
	linuxPCIPath = "/sys/devices/pci0000:00/0000:"
	linuxPCIConfig = "/config"
)

type BiosRange struct {
	Start string `json:"start_dump"`
	End string `json:"end_dump"`
	BitPos uint `json:"bit_pos"`
}

type BiosMap struct {
	LpcBus string `json:"lpc_bus"`
	LpcDev string `json:"lpc_dev"`
	LpcFun string `json:"lpc_fun"`
	BitMap string `json:"bios_dec_off"`
	BitMapSize int `json:"bitmap_size"`
	RangeMap []BiosRange `json:"addresses"`
}

func main() {
	flag.Parse()

	configFile, err := os.Open(*config)
	if err != nil {
		log.Fatal("error while opening: ", *config, " ", err)
	}

	defer configFile.Close()

	buffer, err := ioutil.ReadAll(configFile)
	if err != nil {
		log.Fatal("error while reading buffer from: ", *config, " ", err)
	}

	var biosMap BiosMap

	err = json.Unmarshal(buffer, &biosMap)
	if err != nil {
		log.Fatal("error while parsing config: ", *config, " ", err)
	}

	pciPath := linuxPCIPath + biosMap.LpcBus + ":" + biosMap.LpcDev + "." + biosMap.LpcFun + linuxPCIConfig
	fmt.Println("[+] PCI LPC path:", pciPath)

	bitMask, _ := biosDecRange(pciPath, biosMap)
	biosDump(bitMask, biosMap)
}

func biosDecRange(pciPath string, biosMap BiosMap) (int, error) {
	biosDecOff, err := strconv.ParseUint(biosMap.BitMap, 16, 8)
	if err != nil {
		log.Fatal(err)
	}

	biosDecFile, err := os.Open(pciPath)
	if err != nil {
		log.Fatal("error while opening: ", pciPath, " ", err)
	}

	defer biosDecFile.Close()

	var maskBuff []byte

	switch(biosMap.BitMapSize) {
		case 2:
			maskBuff = make([]byte, 2)
			break
		case 4:
			maskBuff = make([]byte, 4)
			break
		case 8:
			maskBuff = make([]byte, 8)
			break
		default:
			log.Fatal("wrong len for bios_dec_off")
	}

	_, err = biosDecFile.ReadAt(maskBuff, int64(biosDecOff))
	if err != nil {
		log.Fatal(err)
	}

	result := 0

	switch(biosMap.BitMapSize) {
		case 2:
			result = int(binary.BigEndian.Uint16(maskBuff))
			break
		case 4:
			result = int(binary.BigEndian.Uint32(maskBuff))
			break
		case 8: result = int(binary.BigEndian.Uint64(maskBuff))
			break
		default:
			log.Fatal("wrong len for bios_dec_off")
	}

	fmt.Printf("[+] BIOS_DEC_EN1 mask: %x\n", result)

	return result, nil
}

func biosDump(bitMask int, biosMap BiosMap) error {
	if !*probeOnly {
		if _, err := os.Stat(*outDir); os.IsNotExist(err) {
			err := os.MkdirAll(*outDir, os.ModeDir | os.ModePerm)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	for _, addr := range biosMap.RangeMap {
		if (bitMask & (1 << addr.BitPos)) != 0 {
			start, _ := strconv.ParseUint(addr.Start, 16, 64)
			end, _ := strconv.ParseUint(addr.End, 16, 64)
			fmt.Printf("[+] start: 0x%x - end: 0x%x - pos: %d\n", start, end, addr.BitPos)

			if !*probeOnly {
				appendToSegments(start, end)
			}
		}
	}

	if !*probeOnly {
		sortedKeys := sortSegments()

		f, err := os.Create(filepath.Join(*outDir, *outDump))
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()

		totalLen := 0

		for _, v := range sortedKeys {
			length := len(biosSegments[v])
			fmt.Printf("[+] Writing segment: 0x%x - len: %d\n", v, length)
			f.Write(biosSegments[v])
			totalLen += length
			if *saveTemp {
				tmpName := fmt.Sprintf("_%x.dump", v)
				tmp, err := os.Create(filepath.Join(*outDir, tmpName))
				if err != nil {
					log.Fatal(err)
				}
				tmp.Write(biosSegments[v])
				tmp.Close()
			}
		}

		fmt.Printf("Total Dump Length in MiB: %d\n", totalLen)
	}
	return nil
}

func appendToSegments(start, end uint64) error {
	f, err := os.Open(devMem)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	length := end - start + 1
	buffer := make([]byte, length)

	_, err = f.ReadAt(buffer, int64(start))
	if err != nil {
		log.Fatal(err)
	}

	biosSegments[int(start)] = buffer

	return nil
}

func sortSegments() []int {
	var keys []int
	for k := range biosSegments {
		keys = append(keys, int(k))
	}

	sort.Ints(keys)
	return keys
}
