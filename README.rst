GoBiosDump:
===========

- A tool which tries to dump BIOS/UEFI from BIOS_DEC_EN1 ranges which are mapped
  directly onto the SPI bus. The tool reads the PCH/ICH configuration from
  a json config file, then reads PCI-LPC configuration from "/sys/devices/pci..."
  and finally uses this information to dump the BIOS/UEFI from "/dev/mem".
  To add configuration for your PCH/ICH read your datasheet and then use a file
  under the "conf" directory as a starting point.
